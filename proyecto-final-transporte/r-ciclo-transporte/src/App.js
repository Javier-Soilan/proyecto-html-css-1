import './App.css'
import { BrowserRouter as Router } from "react-router-dom";
import { Routes } from "./core/Routes/Routes";
import { PageContext } from './shared/context/PageContext';
import { useState } from 'react';
function App() {
  const [page, setPage] = useState('home')
  return (
    <>
      <Router>
        <PageContext.Provider value={{ page, setPage }}>
          <Routes />
        </PageContext.Provider>
      </Router>
    </>
  )
}

export default App;
