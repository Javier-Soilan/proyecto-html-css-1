import {useForm} from "react-hook-form";
import {NavLink} from "react-router-dom";
import { PageContext } from "../../shared/context/PageContext";

import Rciclo from '../../assets/imgs/Logo_R-CICLO.png'

import './LoginPage.scss';
import { useContext } from "react";

export function LoginPage() {
    const {register, handleSubmit, watch, reset, errors} = useForm();
    const {page, setPage} = useContext(PageContext);

    return(
        <div className="c-login">

            <div className="c-login__logo">
                 <img src={Rciclo} alt=""/>
            </div>

            <div className="c-login__container">

                <div className="c-login__top">
                    <span className="i-truck i-truck--active"/>
                    <h2 className="c-login__tittle">Bienvenido</h2>
                </div>

                <form className="c-login__form">

                    <div className="c-login__mail my-2">
                        
                        <label for="email">Usuario:</label>
                        <input className="b-input b-input--large b-input--white" type="email" id="email" name="mail" placeholder="Correo electrónico" ref={register({required: true, pattern:(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{​​2,4}​​$/)})}/>
                        {errors.mail && <span className="c-contact__span">Introduzca un email valido.</span>}

                    </div>

                    <div className="c-login__password my-2">
                    
                        <label for="password">Contraseña:</label>
                        <input className="b-input b-input--large b-input--white" type="password" id="password" name="password" placeholder="Contraseña" ref={register({required: true, pattern:(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)})}/>
                        {errors.password && <span className="c-contact__span">Contraseña incorrecta.</span>}

                    </div>

                    <div className="c-login__check">

                        <label className="b-checkbox">
                
                            <input className="b-checkbox__box" type="checkbox" name="categories" ref={register({ required: true })}/>
                            <span className="b-checkbox__check"/> Recordar datos.

                        </label>

                     </div>
                    
                     <NavLink  className="c-login__btn" to="/location">
                        <input className="b-btn b-btn--dark" value="Login" onClick={()=>{setPage('location')}}></input>
                    </NavLink>
                
                </form>


            </div>


        </div>
    )
}