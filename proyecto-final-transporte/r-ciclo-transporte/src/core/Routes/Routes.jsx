import { Route, Switch } from 'react-router-dom';

import { LocationPage } from '../../Pages/LocationPage/LocationPage';
import { UserPage } from '../../Pages/UserPage/UserPage'; 
import { LoginPage } from '../../Pages/LoginPage/LoginPage'



export function Routes() {

return(
    <Switch> 
    {/* PRUEBAA */}

        <Route path="/user">
            <UserPage/>
        </Route>

        <Route path="/location">
            <LocationPage/>
        </Route>

        <Route path="/">
            <LoginPage/>
        </Route>    

    </Switch>
);

} 