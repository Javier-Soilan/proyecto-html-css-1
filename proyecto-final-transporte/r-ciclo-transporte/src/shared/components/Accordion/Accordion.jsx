import { useContext, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { PageContext } from '../../context/PageContext'
import './Accordion.scss'
export function Accordion({title}) {
    const {page, setPage} = useContext(PageContext)
    const [open, setOpen] = useState(false)
    const accordion = () => {
        if(open === false){
            setOpen(true)
        } else {
            setOpen(false)
        }
    }



    return (
        <>
            <div className="c-accordion">
                <div className="simulacion_img" ></div>
                
                {/* <img /> */}
                <div className="c-accordion__title" onClick={accordion}> {title}</div>
                {open &&
                    <div>
                        <div className="c-accordion__text">
                            El hospital de koalas en Port Macquarie
                            recibe donaciones para continuar con la ayuda a los cientos de animales que han rescatado.
                        </div>
                        <div className="c-accordion__text c-accordion__text--bigger">
                            <span>7.588.680€</span> recaudados del objetivo de 25.000€
                        </div>
                        <div className="c-accordion__btn">
                            <NavLink to="/donation" onClick={()=>{setPage('donation')}}><button className="b-btn b-btn--dark mb-3">Donar</button></NavLink>
                        </div>
                        <div className="b-btn b-btn--dark b-btn--small my-3" onClick={accordion}>
                        </div>
                    </div>
                }
            </div>
        </>
    )
}