import './MoreInfo.scss'
import { Accordion } from "../Accordion/Accordion";
import { useContext} from 'react';
import { PageContext } from '../../context/PageContext';
import { BackButton } from '../BackButton/BackButton';

export function MoreInfo() {
    const { page, setPage } = useContext(PageContext)

    return (
        <>
            <BackButton backContent='Home'/>

            <div className="c-moreInfo">
                <p>¿Donde puedo donar para ayuadar a Australia?</p>
            </div>
            <div className="c-moreInfo__box my-3">
                <Accordion title="Incendios arrasan Australia" />
            </div>
            <div className="c-moreInfo__box my-3">
                <Accordion title="Bomberos en Australia" />
            </div>
            <div className="c-moreInfo__box my-3">
                <Accordion title="Hospital de Koalas" />
            </div>
        </>
    )
}