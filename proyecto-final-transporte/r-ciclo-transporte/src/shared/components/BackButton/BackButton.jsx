import './BackButton.scss'
import { useContext } from 'react';
import { PageContext } from '../../context/PageContext';
import { NavLink } from 'react-router-dom';

export function BackButton({ backContent }) {

    const { page, setPage } = useContext(PageContext)

    return (
        <>
            { page === 'donation' &&
                <>
                    <NavLink className="c-selector__button" to="/home">
                        <span className="i-back c-backButtonIcon" onClick={() => { setPage('home') }}></span>
                        <span className="c-backButtonBack" onClick={() => { setPage('home') }}>{backContent}</span>
                    </NavLink>
                </>
            }
            {page === 'selector' &&
                <>
                    <NavLink className="c-selector__button" to="/home">
                        <span className="i-back c-backButtonIcon" onClick={() => { setPage('home') }}></span>
                        <span className="c-backButtonBack" onClick={() => { setPage('home') }}>{backContent}</span>
                    </NavLink>
                </>
            }
            {page === 'moreInfo' &&
                <>
                    <NavLink className="c-selector__button" to="/home">
                        <span className="i-back c-backButtonIcon" onClick={() => { setPage('home') }}></span>
                        <span className="c-backButtonBack" onClick={() => { setPage('home') }}>{backContent}</span>
                    </NavLink>
                </>
            }
            {page === 'pickUpForm' &&
                <>
                    <span className="i-back c-backButtonIcon" onClick={() => { setPage('pickUp') }}></span>
                    <span className="c-backButtonBack" onClick={() => { setPage('pickUp') }}>{backContent}</span>
                </>
            }
        </>
    )
}