import { useContext } from "react";
import {NavLink} from "react-router-dom";
import { PageContext } from "../../../shared/context/PageContext";
import './Menu.scss'
export function Menu (){
    const {page, setPage} = useContext(PageContext)
    return(
        <>
                <div className="c-menu">

                    <NavLink className="i-house" activeClassName="i-house--active" exact to="/home" onClick={()=>{setPage('home')}}>
                    </NavLink>
                    <NavLink className="i-truck" activeClassName="i-truck--active" to="/pickUp" onClick={()=>{setPage('pickUp')}}>
                    </NavLink>
                    <NavLink className="i-user" activeClassName="i-user--active" to="/user" onClick={()=>{setPage('user')}}>
                    </NavLink>
                </div>
        </>
    )
}