import React from 'react';
import { Chart } from 'primereact/chart';



export function ChartBar() {

const BarChartDemo = () => {
    const basicData = {
        labels: ['Cartón', 'Vidrio', 'Aceite usado', 'Pilas', 'Ropa'],
        datasets: [
            {
                label: 'Enero',
                backgroundColor: '#c3784c',
                data: [10, 5, 3, 3, 0, 30]
            },
        ]
    };

    const getLightTheme = () => {
        let basicOptions = {
            legend: {
                labels: {
                    fontColor: '#4CC3B5' 
                    //! titulo
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        fontColor: '#4CC3B5'
                    //! eje x valores
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontColor: '#4CC3B5'
                    //! eje y valores
                    }
                }]
            }
        };        

        return {
            basicOptions,
        }
    }

    const { basicOptions } = getLightTheme();

        return (
            <div>
                <div className="card ">
                    <Chart className="g-primereact-chart" type="bar" data={basicData} options={basicOptions} />
                </div>
            </div>
        )
    }
    return(
        <BarChartDemo />
    )
}