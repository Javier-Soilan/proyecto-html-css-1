import './Selector.scss'
import { useContext } from 'react';
import { PageContext } from '../../context/PageContext';
import { NavLink } from 'react-router-dom';
import { BackButton } from '../BackButton/BackButton';
export function Selector() {
    const { page, setPage } = useContext(PageContext)


    return (
        <>
            <BackButton backContent='Home'/>

            <div className="c-selector">
                <p className="c-selector__title">¿Donde puedo donar para ayuadar a Australia?</p>
            </div>
            <div className="c-selector__image">
                <div className="imageSimulator"></div>
            </div>
            <div className="c-selector__button" onClick={() => { setPage('donation') }}>
                <button className="b-btn b-btn--bigWidth mb-3 mt-5">Bomberos</button>
            </div>
            <div className="c-selector__button"  onClick={() => { setPage('donation') }}>
                <button className="b-btn b-btn--bigWidth my-3">Esfuerzo de ayuda y apoyo</button>
            </div>
            <div className="c-selector__button"  onClick={() => { setPage('donation') }}>
                <button className="b-btn b-btn--bigWidth my-3">Ayuda para animales</button>
            </div>
            <div className="c-selector__button"  onClick={() => { setPage('donation') }}>
                <button className="b-btn b-btn--bigWidth my-3">Hogares de emergencia</button>
            </div>
        </>
    )
}