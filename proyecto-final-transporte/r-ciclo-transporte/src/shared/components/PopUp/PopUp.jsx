import { useContext } from 'react'
import { NavLink } from 'react-router-dom';
import { PageContext } from '../../context/PageContext'
import './PopUp.scss'
export function PopUp({ btnContent, btn2Content, fnShowPopUp }) {
    const { page, setPage } = useContext(PageContext)
    return (
        <>
            <div className="c-popUpBackground">
            {page === 'home' && <div className="c-popUpClose i-close" onClick={fnShowPopUp}></div>}
                <div className="c-popUp">
                    <div className="c-popUp__title">
                        
                        {page === 'home' && <span className="i-fire mr-2"></span>}
                        {page === 'home' && <span className="c-popUp__text"> Australia te necesita </span>}
                        {page === 'donation' && <span className="i-tick mr-2"></span>}
                        {page === 'donation' && <span className="c-popUp__text"> Tu donación se ha efectuado con exito</span>}
                        {page === 'pickUpForm' && <span className="i-tick mr-2"></span>}
                        {page === 'pickUpForm' && <span className="c-popUp__text"> ¡Tu recogida ha sido programada con éxito!</span>}
                        
                    </div>
                    {page === 'home' && <div className="c-popUp__image mb-3"></div>}
                    {page === 'home' && 
                    <ul className="c-popUp__uList">
                        <li className="c-popUp__list"><span> En breves sabrás tu aportación</span></li>
                    </ul>
                    }
                    {page === 'donation' && 
                    <ul className="c-popUp__uList">
                        <li className="c-popUp__list"><span> Enviaremos el certificado a tu email.</span></li>
                    </ul>
                    }
                    {page === 'pickUpForm' && 
                    <ul className="c-popUp__uList">
                        <li className="c-popUp__list"><span> Te avisaremos cuando llegue la hora</span></li>
                    </ul>
                    }
                    {page === 'home' && 
                    <NavLink className="c-popUp__btn" to="/donation">
                        <button className="b-btn b-btn--dark" onClick={()=>{setPage('selector')}}>{btnContent}</button>
                    </NavLink>
                    }
                    {page === 'home' && 
                    <NavLink className="c-popUp__btn" to="/donation">
                        <button className="b-btn b-btn--dark mt-3" onClick={()=>{setPage('moreInfo')}}>{btn2Content}</button>
                    </NavLink>
                    }
                    {page === 'donation' && 
                    <NavLink className="c-popUp__btn" to="/home">
                        <button className="b-btn b-btn--dark" onClick={()=>{setPage('home')}}>{btnContent}</button>
                    </NavLink>
                    }
                    {page === 'pickUpForm' && 
                    <NavLink className="c-popUp__btn" to="/home">
                        <button className="b-btn b-btn--dark" onClick={()=>{setPage('home')}}>{btnContent}</button>
                    </NavLink>
                    }

                    
                </div>
            </div>
        </>
    )
}